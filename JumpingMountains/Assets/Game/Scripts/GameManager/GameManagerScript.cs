using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class GameManagerScript : MonoBehaviour
{
    #region Singleton

    public static GameManagerScript instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    public AudioSource audio;
    public AudioClip painSound, pickUpHPSound, pickUpFilesSound;

    public Transform respawn;
    public Vector3 limits;

    public GameObject spawnManager;

    public int maxKilledEnemies, currentKilledEnemies, maxCollectedFiles, currentCollectedFilles, currentPortalSabotage, maxPortalSabotage, currentCaptainKilled, maxCaptainKilled, maxHP, currentHP;

    public bool win = false, lost = false;
    private bool filesCollected = false, allEnemiesDestroyed = false, captainKilled = false, portalSabotaged = false;

    void Start()
    {
        audio = GetComponent<AudioSource>();

        currentHP = maxHP;

        respawn = GameObject.Find("Limit").transform;
        limits.y =-80f;
    }

    void Update()
    {
        Files();
        EnemiesDestroyed();
        Captain();
        Sabotage();
        WinCondition();


        ResetPos();
        TeleportToOrinigalPos();
        DestroyPlayer();
    }
    private void EnemiesDestroyed()
    {
        if (currentKilledEnemies >= maxKilledEnemies)
        {
            allEnemiesDestroyed = true;
        }
    }
    private void Files()
    {
        if (currentCollectedFilles == maxCollectedFiles)
        {
            filesCollected = true;
        }
    }
    private void Captain()
    {
        if (currentCaptainKilled == maxCaptainKilled)
        {
            captainKilled = true;
        }
    }
    private void Sabotage()
    {
        if (currentPortalSabotage == maxPortalSabotage)
        {
            portalSabotaged = true;
        }
    }
    private void WinCondition()
    {
        if (filesCollected && captainKilled && allEnemiesDestroyed && portalSabotaged)
        {
            Instantiate(spawnManager, this.transform.position, this.transform.rotation);
            win = true;
        }
    }
    private void ResetPos()
    {
        if(this.transform.position.y < limits.y)
        {
            this.transform.position = respawn.position;
        }
    }
    private void TeleportToOrinigalPos()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            this.transform.position = respawn.position;
        }
    }
    private void DestroyPlayer()
    {
        if (currentHP <= 0)
        {
            Instantiate(spawnManager, this.transform.position, this.transform.rotation);
            lost = true;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Files")
        {
            currentCollectedFilles += 1;
            audio.PlayOneShot(pickUpFilesSound);
            Destroy(other.gameObject);
        }
        if (other.tag == "HP")
        {
            audio.PlayOneShot(pickUpHPSound);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "EnemyBullet")
        {
            currentHP -= CommonEnemy.instance.damage;
            audio.PlayOneShot(painSound);
        }
        if (collision.gameObject.tag == "CaptainBullet")
        {
            currentHP -= BossEnemy.instance.damage;
            audio.PlayOneShot(painSound);
        }
    }
}
