using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class MissionConsole : MonoBehaviour
{
    #region Singleton

    public static MissionConsole instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    public AudioSource audio;
    public AudioClip consoleSound, activePortalSound;

    public Text timer;

    
    public GameObject file, activeTextPanel, luzDeConsola;
    private Transform filesSpawnPoint;

    public int cantidad;

    public float timerToComplete;

    public bool canActiveMission, missionIsActive, missionComplete;

    void Start()
    {
        filesSpawnPoint = GameObject.Find("filesSpawnPos").transform;

        timer.gameObject.SetActive(false);
        activeTextPanel.SetActive(false);
    }


    void Update()
    {
        ActivateMission();
        ActiveTimer();
        MissionCompleted();
    }
    private void ActivateMission()
    {
        if (canActiveMission && Input.GetKeyDown(KeyCode.F))
        {
            audio.PlayOneShot(consoleSound);
            audio.PlayOneShot(activePortalSound);
            activeTextPanel.SetActive(false);
            luzDeConsola.SetActive(false);
            missionIsActive = true;
        }
    }
    private void ActiveTimer()
    {
        if (missionIsActive)
        {
            timer.gameObject.SetActive(true);

            timerToComplete -= Time.deltaTime;
            timer.text = "Times left to complete the sabotage: " + timerToComplete.ToString("0");

            if (timerToComplete <= 0)
            {
                missionComplete = true;

                timer.gameObject.SetActive(false);
                timerToComplete = 60f;
            }
        }
    }
    private void MissionCompleted()
    {
        if (missionComplete)
        {
            missionIsActive = false;
            GameManagerScript.instance.currentPortalSabotage = 1;

            if (cantidad == 0)
            {
                Instantiate(file, filesSpawnPoint.position, filesSpawnPoint.rotation);
                cantidad++;
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            if (!missionIsActive && !missionComplete)
            {
                activeTextPanel.SetActive(true);
                canActiveMission = true;
            }
            else
            {
                activeTextPanel.SetActive(false);
                canActiveMission = false;
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            if (!missionIsActive && !missionComplete)
            {
                activeTextPanel.SetActive(false);
                canActiveMission = false;
            }
        }
    }
}
