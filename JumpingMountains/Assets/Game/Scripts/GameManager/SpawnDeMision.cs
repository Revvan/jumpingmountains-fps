using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnDeMision : MonoBehaviour
{
    public GameObject enemiesToSpawn;
    public float timeToSpawn2, timeToSpawn3;

    public Transform spawnPos2, spawnPos3;
    private Vector3 spawn2, spawn3;
    void Start()
    {
        spawn2 = spawnPos2.position;
        spawn3 = spawnPos3.position;
    }

    void Update()
    {
        if (MissionConsole.instance.missionIsActive)
        {
            SpawnEnemiesTwo(); SpawnEnemiesThree();
        }
        else
        {
            timeToSpawn2 = 10f;
            timeToSpawn3 = 10f;
        }
    }
    private void SpawnEnemiesTwo()
    {
        timeToSpawn2 -= Time.deltaTime;

        if (timeToSpawn2 <= 0)
        {
            Instantiate(enemiesToSpawn, spawn2, enemiesToSpawn.transform.rotation);

            timeToSpawn2 = 10f;
        }
    }
    private void SpawnEnemiesThree()
    {
        timeToSpawn3 -= Time.deltaTime;

        if (timeToSpawn3 <= 0)
        {
            Instantiate(enemiesToSpawn, spawn3, enemiesToSpawn.transform.rotation);

            timeToSpawn3 = 10f;
        }
    }
}
