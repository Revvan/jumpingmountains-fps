using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagerScript : MonoBehaviour
{
    #region Singleton

    public static SceneManagerScript instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    public GameObject myManager;

    public bool win, lost;
    private void Start()
    {
        if (GameManagerScript.instance.win)
        {
            win = true;
            SceneManager.LoadScene(2);
            DontDestroyOnLoad(myManager);
        }
        if (GameManagerScript.instance.lost)
        {
            lost = true;
            SceneManager.LoadScene(2);
            DontDestroyOnLoad(myManager);
        }
    }
}
