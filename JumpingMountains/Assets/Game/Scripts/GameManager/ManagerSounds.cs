using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;


public class ManagerSounds : MonoBehaviour
{
    public AudioSource ambienceSource, musicSource;

    public AudioClip ambience, music;

    void Start()
    {
        ambienceSource = GetComponent<AudioSource>();

        ambienceSource.clip = ambience;
        musicSource.clip = music;
        ambienceSource.Play();
        musicSource.Play();
    }

    
    void Update()
    {
        if (ManagerHUD.instance.pauseActive)
        {
            ambienceSource.Pause();
            musicSource.Pause();
        }
        else
        {
            ambienceSource.UnPause();
            musicSource.UnPause();
        }
    }
}
