using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class HPBox : MonoBehaviour
{
    private Transform player;
    

    public int giveHP, calculateHeal, umbral;

    public bool belowUmbralHealth, superiorUmbralHealth;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }
    void Update()
    {
        umbral = (int)(GameManagerScript.instance.maxHP * 0.8);

        if (GameManagerScript.instance.currentHP < GameManagerScript.instance.maxHP)
        {
            calculateHeal = GameManagerScript.instance.maxHP - GameManagerScript.instance.currentHP;
        }

        if (GameManagerScript.instance.currentHP <= umbral)
        {
            belowUmbralHealth = true;
        }
        else
        {
            belowUmbralHealth = false;
        }

        if (GameManagerScript.instance.currentHP > umbral)
        {
            superiorUmbralHealth = true;
        }
        else
        {
            superiorUmbralHealth = false;
        }

        if (GameManagerScript.instance.currentHP == GameManagerScript.instance.maxHP)
        {
            belowUmbralHealth = false;
            superiorUmbralHealth = false;
        }

        AutoLooker();
    }
    private void AutoLooker()
    {
        Vector3 direccion = (player.position - transform.position).normalized;
        Quaternion mirar = Quaternion.LookRotation(new Vector3(direccion.x, direccion.y, direccion.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, mirar, Time.deltaTime * 2);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (belowUmbralHealth)
            {
                GameManagerScript.instance.currentHP += giveHP;
            }
            if (superiorUmbralHealth)
            {
                GameManagerScript.instance.currentHP += calculateHeal;
            }
            if (GameManagerScript.instance.currentHP >= GameManagerScript.instance.maxHP)
            {
                GameManagerScript.instance.currentHP += 0;
            }


            Destroy(this.gameObject);
        }
    }
}
