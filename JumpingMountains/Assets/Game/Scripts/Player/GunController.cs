using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class GunController : MonoBehaviour
{
    #region Singleton;

    public static GunController instance;

    #endregion;

    public Camera fpsCam;
    public Transform attackPoint;
    public GameObject bullet, muzzleFlash;

    public AudioSource audio;
    public AudioClip shootSound, reloadSound, chargeSound;


    public float shootForce, upwardForce, timeBetweenShooting, spread, reloadTime, timeBetweenShots;

    public int magazineSize, bulletPerTap, bulletsLeft;
    private int  bulletsShot, noMoreBullets, difB, actualMaxAmmo, maxAmmo;


    public bool allowButtonHold;
    private bool allowInvoke = true, noAmmo, shooting, readyToShoot, reloading;

    private void Awake()
    {
        instance = this;
        bulletsLeft = magazineSize;

        maxAmmo = magazineSize;
        //actualMaxAmmo = maxAmmo;

        readyToShoot = true;
    }
    private void Start()
    {
        audio = GetComponent<AudioSource>();
    }
    private void Update()
    {
        MyInput();
    }
    private void MyInput()
    {
        if (!ManagerHUD.instance.pauseActive)
        {
            if (allowButtonHold)
            {
                shooting = Input.GetKey(KeyCode.Mouse0);
            }
            else
            {
                shooting = Input.GetKeyDown(KeyCode.Mouse0);
            }

            if (Input.GetKeyDown(KeyCode.R) && bulletsLeft < magazineSize && !reloading)
            {
                Reload();
            }
            if (readyToShoot && shooting && !reloading && bulletsLeft <= 0)
            {
                Reload();
            }

            if (readyToShoot && shooting && !reloading && bulletsLeft > 0)
            {
                bulletsShot = 0;

                Shoot();
            }
        }



        //if (actualMaxAmmo <= 0)
        //{
        //    actualMaxAmmo = 0;
        //    noAmmo = true;
        //}
        //else
        //{
        //    noAmmo = false;
        //}
    }
    private void Shoot()
    {
        readyToShoot = false;

        Ray ray = fpsCam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));
        RaycastHit hit;
        Vector3 targetPoint;
        if (Physics.Raycast(ray, out hit))
        {
            targetPoint = hit.point;
        }
        else
        {
            targetPoint = ray.GetPoint(75);
        }

        Vector3 directionWithOutSpread = targetPoint - attackPoint.position;


        float x = Random.Range(-spread, spread);
        float y = Random.Range(-spread, spread);

        Vector3 directionWithSpread = directionWithOutSpread + new Vector3(x, y, 0);

        GameObject currentBullet = Instantiate(bullet, attackPoint.position, Quaternion.identity);
        currentBullet.transform.forward = directionWithSpread.normalized;
        audio.PlayOneShot(shootSound);


        Destroy(currentBullet, 3f);

        currentBullet.GetComponent<Rigidbody>().AddForce(directionWithSpread.normalized * shootForce, ForceMode.Impulse);
        currentBullet.GetComponent<Rigidbody>().AddForce(fpsCam.transform.up * upwardForce, ForceMode.Impulse);

        if (muzzleFlash != null)
        {
            GameObject currentEffect = Instantiate(muzzleFlash, attackPoint.position, muzzleFlash.transform.rotation);
            Destroy(currentEffect, 0.1f);
        }

        bulletsLeft--;
        bulletsShot++;


        if (allowInvoke)
        {
            Invoke("ResetShot", timeBetweenShooting);
            allowInvoke = false;
        }

        if (bulletsShot < bulletPerTap && bulletsLeft > 0)
        {
            Invoke("Shoot", timeBetweenShots);
        }
    }
    private void ResetShot()
    {
        readyToShoot = true;
        allowInvoke = true;
    }
    private void Reload()
    {
        if (noAmmo == false)
        {
            reloading = true;
            audio.PlayOneShot(reloadSound);
            Invoke("ReloadFinished", reloadTime);
        }
        else
        {
            reloading = false;
            CancelInvoke("ReloadFinished");
        }
    }
    private void ReloadFinished()
    {
        if (bulletsLeft <= 0)
        {
            audio.PlayOneShot(chargeSound);
            bulletsLeft = magazineSize;
            //actualMaxAmmo -= bulletsLeft;
        }
        if (bulletsLeft >= 0)
        {
            audio.PlayOneShot(chargeSound);
            bulletsLeft = magazineSize;
        }
        //if (bulletsLeft > 0)
        //{
        //    noMoreBullets = magazineSize - bulletsLeft;

        //    if (noMoreBullets > actualMaxAmmo)
        //    {
        //        difB = actualMaxAmmo;
        //        bulletsLeft += actualMaxAmmo;
        //        actualMaxAmmo -= difB;
        //    }
        //    else
        //    {
        //        bulletsLeft += noMoreBullets;
        //        actualMaxAmmo -= noMoreBullets;
        //    }
        //}
        reloading = false;
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(attackPoint.position, attackPoint.forward);
    }
}
