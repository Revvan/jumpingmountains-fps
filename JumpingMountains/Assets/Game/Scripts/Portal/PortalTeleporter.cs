using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class PortalTeleporter : MonoBehaviour
{
    public AudioSource audio;
    public AudioClip portalSound;

    public Transform player, receiver;

    public bool playerIsOverlapping = false;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;

        audio = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (playerIsOverlapping)
        {
            Vector3 portalToPlayer = player.position - transform.position;
            float dotProduct = Vector3.Dot(transform.up, portalToPlayer);

            if (dotProduct < 0f)
            {
                float RotationDifference = Quaternion.Angle(transform.rotation, receiver.rotation);
                RotationDifference += 180;
                player.Rotate(Vector3.up, RotationDifference);

                Vector3 positionOffSet = Quaternion.Euler(0f, RotationDifference, 0f) * portalToPlayer;
                player.position = receiver.position - positionOffSet;

                playerIsOverlapping = false;
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            audio.PlayOneShot(portalSound);
            playerIsOverlapping = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            playerIsOverlapping = false;
        }
    }
}
