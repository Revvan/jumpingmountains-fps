using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalScript : MonoBehaviour
{
    public Transform playerCam, portalB, portalA;


    void Update()
    {
        Vector3 playerOffSetFromPortal = playerCam.position - portalA.position;
        transform.position = portalB.position + playerOffSetFromPortal;

        float angleDifferenceBetweenPortalRotations = Quaternion.Angle(portalB.rotation, portalA.rotation);
        Quaternion portalRotationalDifference = Quaternion.AngleAxis(angleDifferenceBetweenPortalRotations, Vector3.up);
        Vector3 newCamDirection = portalRotationalDifference * playerCam.forward;
        transform.rotation = Quaternion.LookRotation(newCamDirection, Vector3.up);
    }
}
