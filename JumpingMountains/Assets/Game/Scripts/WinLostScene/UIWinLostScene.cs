using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIWinLostScene : MonoBehaviour
{
    public GameObject retryB, quitB, winPanel, lostPanel;

    public int win;

    void Start()
    {
        retryB.SetActive(false);
        quitB.SetActive(false);
        winPanel.SetActive(false);
        lostPanel.SetActive(false);

        Time.timeScale = 1;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }
    void Update()
    {
        WinCondition();
        LostCondition();
    }
    private void WinCondition()
    {
        if (SceneManagerScript.instance.win)
        {
            retryB.SetActive(true);
            quitB.SetActive(true);
            winPanel.SetActive(true);

            //if (SceneManagerScript.instance.win)
            //{
            //    Interlude.InterludeManager.CanFindKey();

            //    if(win == 0)
            //    {
            //        Interlude.InterludeManager.KeyFound();
            //        win = 1;
            //    }
            //}

        }
    }
    private void LostCondition()
    {
        if (SceneManagerScript.instance.lost)
        {
            retryB.SetActive(true);
            quitB.SetActive(true);
            lostPanel.SetActive(true);
        }
    }
    public void RetryGame()
    {
        SceneManager.LoadScene(1);
    }
    public void CloseGame()
    {
        Application.Quit();
    }
}
