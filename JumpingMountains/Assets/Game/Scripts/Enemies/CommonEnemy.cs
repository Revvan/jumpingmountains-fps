using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Audio;
using UnityEngine.Animations;


[RequireComponent(typeof(NavMeshAgent))]
public class CommonEnemy : MonoBehaviour
{
    #region Singleton

    public static CommonEnemy instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    Animator anim;
    NavMeshAgent agent;

    public AudioSource audio;
    public AudioClip shootSound, painSound;

    public Transform player, target, canon, boxPos;
    public GameObject bullet, muzzleFlash, HPpack;
    public float gizmoRangeDetection, gizmoRangeAttack, shootForce;


    public int maxHP, damage;
    private int currentHP;

    public EnemyHealthBar healthBar;
    void Start()
    {
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        audio = GetComponent<AudioSource>();

        player = GameObject.FindGameObjectWithTag("Player").transform;

        currentHP = maxHP;
        healthBar.SetMaxHealth(maxHP);
    }

    void Update()
    {
        healthBar.SetHealth(currentHP);

        CalculateEnemyPos();
        DestroyEnemy();
    }
    #region Movement
    private void CalculateEnemyPos()
    {
        gizmoRangeAttack = agent.stoppingDistance;
        float distance = Vector3.Distance(this.transform.position, player.position);

        if (target != null)
        {
            anim.SetBool("idle", false);
            anim.SetBool("walk", true);
            anim.SetBool("fire", false);
        }
        else
        {
            anim.SetBool("idle", true);
            anim.SetBool("walk", false);
            anim.SetBool("fire", false);
        }

        if (distance < gizmoRangeDetection)
        {
            target = player;
            agent.stoppingDistance = 15f;
            agent.SetDestination(target.position);
            AutoLooker();

            if (distance < gizmoRangeAttack)
            {
                AutoLooker();
                anim.SetBool("idle", false);
                anim.SetBool("walk", false);
                anim.SetBool("fire", true);
            }
            else
            {
                anim.SetBool("idle", false);
                anim.SetBool("walk", true);
                anim.SetBool("fire", false);
            }
        }
        else
        {
            anim.SetBool("idle", true);
            anim.SetBool("walk", false);
            anim.SetBool("fire", false);
            target = null;
            agent.stoppingDistance = 0f;
        }
    }
    private void AutoLooker()
    {
        Vector3 direccion = (player.position - transform.position).normalized;
        Quaternion mirar = Quaternion.LookRotation(new Vector3(direccion.x, direccion.y, direccion.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, mirar, Time.deltaTime * 2);
    }
    #endregion

    #region Attack
    public void Attack()
    {
        GameObject currentBullet = Instantiate(bullet, this.canon.transform.position, canon.transform.rotation);
        GameObject currentMuzzleFlash = Instantiate(muzzleFlash, this.canon.transform.position, canon.transform.rotation);
        Rigidbody rb = currentBullet.GetComponent<Rigidbody>();
        rb.AddForce(this.canon.transform.forward * shootForce, ForceMode.Impulse);
        audio.PlayOneShot(shootSound);
        Destroy(currentBullet, 1.5f);
        Destroy(currentMuzzleFlash, 0.1f);
    }
    #endregion

    #region Stats
    private void DestroyEnemy()
    {
        if (currentHP <= 0)
        {
            anim.SetBool("idle", false);
            anim.SetBool("walk", false);
            anim.SetBool("fire", false);
            anim.SetBool("dead", true);
            anim.SetInteger("death", Random.Range(0, 3));

            GameManagerScript.instance.currentKilledEnemies++;

            gameObject.GetComponent<CommonEnemy>().enabled = false;
        }
    }
    public void StopDeathAnim()
    {
        Instantiate(HPpack, boxPos.position, HPpack.transform.rotation);

        Destroy(this.gameObject);
    }
    #endregion
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(this.transform.position, gizmoRangeDetection);

        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(this.transform.position, gizmoRangeAttack);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "PlayerBullet")
        {
            currentHP -= PlayerBulletScript.instance.damage;
            healthBar.SetHealth(currentHP);
            audio.PlayOneShot(painSound);
        }
    }
}
