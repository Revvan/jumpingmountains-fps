using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Audio;
using UnityEngine.Animations;


[RequireComponent(typeof(NavMeshAgent))]
public class BossEnemy : MonoBehaviour
{
    #region Singleton

    public static BossEnemy instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion;

    Animator anim;
    NavMeshAgent agent;

    public AudioSource audio;
    public AudioClip shootSound, painSound;

    public GameObject file, bullet, muzzleFlash;

    public Transform player, canon;
    private Transform target;

    private int cantidad;
    public int maxHP, currentHP, damage;


    public float gizmoRangeDetection, shootForce;
    private float gizmoRangeAttack;

    public EnemyHealthBar healthBar;
    void Start()
    {
        anim = GetComponent<Animator>();
        audio = GetComponent<AudioSource>();
        agent = GetComponent<NavMeshAgent>();
        

        player = GameObject.FindGameObjectWithTag("Player").transform;

        
        currentHP = maxHP;
        healthBar.SetMaxHealth(maxHP);
    }

    void Update()
    {
        CalculateDistanceWithPlayer();

        ActiveDeathAnimation();

        healthBar.SetHealth(currentHP);
    }
    private void CalculateDistanceWithPlayer()
    {
        float distance = Vector3.Distance(this.transform.position, player.position);

        if (distance < gizmoRangeDetection)
        {
            AutoLooker();
            target = player;
            agent.SetDestination(target.position);
            agent.stoppingDistance = 15f;
            gizmoRangeAttack = agent.stoppingDistance;


            if (distance <= gizmoRangeAttack)
            {
                AutoLooker();
                anim.SetBool("idle", false);
                anim.SetBool("move", false);
                anim.SetBool("fire", true);
            }
            else
            {
                anim.SetBool("idle", false);
                anim.SetBool("move", true);
                anim.SetBool("fire", false);
            }
        }
        else
        {
            target = null;
            agent.stoppingDistance = 0f;
            gizmoRangeAttack = agent.stoppingDistance;

            anim.SetBool("idle", true);
            anim.SetBool("move", false);
            anim.SetBool("fire", false);
        }
    }
    private void AutoLooker()
    {
        Vector3 direccion = (player.position - transform.position).normalized;
        Quaternion mirar = Quaternion.LookRotation(new Vector3(direccion.x, direccion.y, direccion.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, mirar, Time.deltaTime * 2);
    }
    public void Attack()
    {
        GameObject currentBullet = Instantiate(bullet, this.canon.transform.position, canon.transform.rotation);
        GameObject currentMuzzleFlash = Instantiate(muzzleFlash, this.canon.transform.position, canon.transform.rotation);
        Rigidbody rb = currentBullet.GetComponent<Rigidbody>();
        rb.AddForce(this.canon.transform.forward * shootForce, ForceMode.Impulse);
        audio.PlayOneShot(shootSound);
        Destroy(currentBullet, 1.5f);
        Destroy(currentMuzzleFlash, 0.1f);
    }
    private void ActiveDeathAnimation()
    {
        if (currentHP <= 0)
        {
            if (cantidad == 0)
            {
                Instantiate(file, this.transform.position, this.transform.rotation);
                cantidad = 1;
            }

            anim.SetBool("idle", false);
            anim.SetBool("move", false);
            anim.SetBool("fire", false);
            anim.SetBool("dead", true);

            GameManagerScript.instance.currentCaptainKilled = 1;

            gameObject.GetComponent<BossEnemy>().enabled = false;
        }
    }
    public void DestroyCaptain()
    {
        Destroy(this.gameObject);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "PlayerBullet")
        {
            currentHP -= PlayerBulletScript.instance.damage;
            healthBar.SetHealth(currentHP);
            audio.PlayOneShot(painSound);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(this.transform.position, gizmoRangeDetection);

        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(this.transform.position, gizmoRangeAttack);
    }
}
