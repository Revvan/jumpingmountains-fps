using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanonTarget : MonoBehaviour
{
    #region Singleton

    public static CanonTarget instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    public Transform player;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }
    void Update()
    {
        AutoLooker();
    }
    private void AutoLooker()
    {
        Vector3 direccion = (player.position - transform.position).normalized;
        Quaternion mirar = Quaternion.LookRotation(new Vector3(direccion.x, direccion.y, direccion.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, mirar, Time.deltaTime * 2);
    }
}
