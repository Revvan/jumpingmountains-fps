using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ManagerHUD : MonoBehaviour
{
    #region Singleton

    public static ManagerHUD instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    #region MenuVariables
    public GameObject panel, panelBlur, pauseT, resumeT, reloadB, controls;
    public bool pauseActive;
    #endregion

    #region MisionesVariables

    public Text killCount, filesCount, portalSabotage, slainCaptain;

    public GameObject checkMark, checkMark1, checkMark2, checkMark3;

    #endregion

    #region PlayerHUD
    public Text hPT, ammoT;
    #endregion
    void Start()
    {
        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        checkMark.SetActive(false);
        checkMark1.SetActive(false);
        checkMark2.SetActive(false);
        checkMark3.SetActive(false);
    }
    void Update()
    {
        PauseIsActive();

        MissionPanel();

        PlayerHUD();
    }
    private void PlayerHUD()
    {
        hPT.text = "HP: " + GameManagerScript.instance.currentHP.ToString() + " / " + GameManagerScript.instance.maxHP.ToString();
        ammoT.text = "Ammo: " + GunController.instance.bulletsLeft.ToString() + " / " + Mathf.Infinity.ToString();
    }

    private void MissionPanel()
    {
        killCount.text = "Enemies killed: " + GameManagerScript.instance.currentKilledEnemies.ToString() + " / " + GameManagerScript.instance.maxKilledEnemies.ToString();
        filesCount.text = "Files collected: " + GameManagerScript.instance.currentCollectedFilles.ToString() + " / " + GameManagerScript.instance.maxCollectedFiles.ToString();
        portalSabotage.text = "Portal sabotaged: " + GameManagerScript.instance.currentPortalSabotage.ToString() + " / " + GameManagerScript.instance.maxPortalSabotage.ToString();
        slainCaptain.text = "Captain killed: " + GameManagerScript.instance.currentCaptainKilled.ToString() + " / " + GameManagerScript.instance.maxCaptainKilled.ToString();

        if (GameManagerScript.instance.currentKilledEnemies >= GameManagerScript.instance.maxKilledEnemies)
        {
            checkMark.SetActive(true);
        }
        if (GameManagerScript.instance.currentCollectedFilles >= GameManagerScript.instance.maxCollectedFiles)
        {
            checkMark1.SetActive(true);
        }
        if (GameManagerScript.instance.currentPortalSabotage >= GameManagerScript.instance.maxPortalSabotage)
        {
            checkMark2.SetActive(true);
        }
        if (GameManagerScript.instance.currentCaptainKilled >= GameManagerScript.instance.maxCaptainKilled)
        {
            checkMark3.SetActive(true);
        }
    }

    private void PauseIsActive()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PauseGame();
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        if (pauseActive)
        {
            panel.SetActive(true);
            panelBlur.SetActive(true);
            pauseT.SetActive(true);
            resumeT.SetActive(true);
            reloadB.SetActive(true);
            controls.SetActive(true);
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

            panel.SetActive(false);
            panelBlur.SetActive(false);
            pauseT.SetActive(false);
            resumeT.SetActive(false);
            reloadB.SetActive(false);
            controls.SetActive(false);
        }
    }
    public void ReloadGame()
    {
        SceneManager.LoadScene(1);
    }
    private void PauseGame()
    {
        if (Time.timeScale == 1)
        {

            pauseActive = true;
            Time.timeScale = 0;
        }
        else
        {
            reloadB.SetActive(false);
            pauseActive = false;
            Time.timeScale = 1;
        }
    }
}
